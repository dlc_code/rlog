#include "rlog_adapter.h"
#include "rtthread.h"

static struct rt_mutex mutex;

#define RLOG_UART_NAME       "uart2"
static rt_device_t serial;

void rlog_lock(void)
{
    rt_mutex_take(&mutex, RT_WAITING_FOREVER);
}

void rlog_unlock(void)
{
    rt_mutex_release(&mutex);
}

char *rlog_get_time(void)
{
    return "2023-8-14";
}

void rlog_output(const char *log, uint16_t len)
{
    rt_device_write(serial, 0, log, len);
}

void rlog_osc_output(const char *data, uint16_t len)
{
    rt_device_write(serial, 0, data, len);
}

void rlog_adapter_init(void)
{
    serial = rt_device_find(RLOG_UART_NAME);
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX);
    
    rt_mutex_init(&mutex, "rlog", RT_IPC_FLAG_PRIO);
}

void rlog_adapter_deinit(void)
{
    rt_mutex_detach(&mutex);
}
