#ifndef __RLOG_ADAPTER_H__
#define __RLOG_ADAPTER_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>

/* Enable log output */
#define RLOG_OUTPUT_ENABLE              1

/* Set log output level, rang: from RLOG_LEVEL_ASSERT to RLOG_LEVEL_VERBOSE */
#define RLOG_OUTPUT_LEVEL               RLOG_LEVEL_VERBOSE

/* Enable log color */
#define RLOG_COLOUR_ENABLE              1

/* Enable log color */
#define RLOG_TIME_ENABLE                1

/* Support log include directory */
#define RLOG_DIRECTORY_ENABLE           1

/* Support log include funtiong name */
#define RLOG_FUNCTION_ENABLE            1

/* Support log include line number*/
#define RLOG_LINE_ENABLE                1

/* Buffer size for every line's log */
#define RLOG_LINE_BUFF_LEN              128

/* Output line number max length */
#define RLOG_LINE_NUM_SIZE              5

/* Output newline sign */
#define RLOG_NEWLINE_SIGN               "\r\n"

/* Enable assert check */
#define RLOG_ASSERT_ENABLE              1

/* Log function. default FDB_PRINT macro is printf() */
#define RLOG_PRINT(...)                 printf(__VA_ARGS__)

void rlog_adapter_init(void);

void rlog_adapter_deinit(void);

void rlog_lock(void);

void rlog_unlock(void);

char *rlog_get_time(void);

void rlog_output(const char *log, uint16_t len);

void rlog_osc_output(const char *data, uint16_t len);

#endif
