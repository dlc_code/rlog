#ifndef __RLOG_DEF_H__
#define __RLOG_DEF_H__

#include "rlog.h"

#define RLOG_VERSION_MAJOR              1
#define RLOG_VERSION_MINOR              0
#define RLOG_VERSION_PATCH              0

/* Enable log output */
#ifndef RLOG_OUTPUT_ENABLE
    #define RLOG_OUTPUT_ENABLE          0
#endif

/* Set log output level, rang: from RLOG_LEVEL_ASSERT to RLOG_LEVEL_VERBOSE */
#ifndef RLOG_OUTPUT_LEVEL
    #define RLOG_OUTPUT_LEVEL           RLOG_LEVEL_VERBOSE
#endif

/* Enable log color */
#ifndef RLOG_COLOUR_ENABLE
    #define RLOG_COLOUR_ENABLE          0
#endif

/* Enable log color */
#ifndef RLOG_TIME_ENABLE
    #define RLOG_TIME_ENABLE            0
#endif

/* Support log include directory */
#ifndef RLOG_DIRECTORY_ENABLE
    #define RLOG_DIRECTORY_ENABLE       0
#endif

/* Support log include funtiong name */
#ifndef RLOG_FUNCTION_ENABLE
    #define RLOG_FUNCTION_ENABLE        0
#endif

/* Support log include line number*/
#ifndef RLOG_LINE_ENABLE
    #define RLOG_LINE_ENABLE            0
#endif

/* Buffer size for every line's log */
#ifndef RLOG_LINE_BUFF_LEN
    #define RLOG_LINE_BUFF_LEN          128
#endif

/* Output line number max length */
#ifndef RLOG_LINE_NUM_SIZE
    #define RLOG_LINE_NUM_SIZE          5
#endif

/* Output newline sign */
#ifndef RLOG_NEWLINE_SIGN
    #define RLOG_NEWLINE_SIGN           "\r\n"
#endif

/* Enable assert check */
#ifndef RLOG_ASSERT_ENABLE
    #define RLOG_ASSERT_ENABLE          0
#endif

/* Log function. default FDB_PRINT macro is printf() */
#ifndef RLOG_PRINT
    #define RLOG_PRINT(...)             printf(__VA_ARGS__)
#endif

/* Output log color */
#define RLOG_COLOUR_START               "\033["

#define RLOG_COLOUR_BLACK               "30m"
#define RLOG_COLOUR_RED                 "31m"
#define RLOG_COLOUR_GREEN               "32m"
#define RLOG_COLOUR_YELLOW              "33m"
#define RLOG_COLOUR_BLUE                "34m"
#define RLOG_COLOUR_MAGENTA             "35m"
#define RLOG_COLOUR_CYAN                "36m"
#define RLOG_COLOUR_WHITE               "37m"

#define RLOG_COLOUR_END                 "\033[0m"
#define RLOG_COLOUR_END_SIZE            (sizeof(RLOG_COLOUR_END) -1)

#define RLOG_COLOUR_ASSERT              RLOG_COLOUR_MAGENTA
#define RLOG_COLOUR_ERROR               RLOG_COLOUR_RED
#define RLOG_COLOUR_WARN                RLOG_COLOUR_YELLOW
#define RLOG_COLOUR_INFO                RLOG_COLOUR_CYAN
#define RLOG_COLOUR_DEBUG               RLOG_COLOUR_WHITE
#define RLOG_COLOUR_VERBOSE             RLOG_COLOUR_GREEN

typedef struct
{
    bool output_en;
    bool color_en;
    rlog_lvl_t filter_lvl;
    int level_fmt[RLOG_LVL_MAX];
}rlog_t;

typedef struct
{
    uint8_t head;
    rlog_osc_data data;
    uint8_t checksum;
    uint8_t tail;
}rlog_osc_packet;

int rlog_strcpy(uint16_t cur_len, char *dst, const char *src);

int rlog_snprintf(char *buf, uint16_t size, const char *format, ...);

int rlog_vsnprintf(char *buf, uint16_t size, const char *fmt, va_list args);

#endif
