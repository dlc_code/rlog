#include "rlog.h"

int main()
{
    rlog_init();

    rlog_enable(true);
    rlog_color_enable(true);
    rlog_level_fmt_set(RLOG_LVL_INFO, RLOG_FMT_ALL);
    rlog_print("RiceChen\r\n");

    rlog_i("RiceChen");
    rlog_hexdump_print("rice", "RiceChen", strlen("RiceChen"));

    return 0;
}
