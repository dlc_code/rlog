#include "rlog_adapter.h"
#include "rlog_def.h"
#include "pthread.h"
#include <time.h>

static pthread_mutex_t mutex;

void rlog_lock(void)
{
    pthread_mutex_lock(&mutex);
}

void rlog_unlock(void)
{
    pthread_mutex_unlock(&mutex);
}

char *rlog_get_time(void)
{
#define TIME_STR_SIZE       32
    static char time_str[TIME_STR_SIZE] = {0};
    time_t tmp;
    struct tm *timp;

    time(&tmp);   
    timp = localtime(&tmp);

    memset(time_str, 0, TIME_STR_SIZE);
    rlog_snprintf(time_str, TIME_STR_SIZE, "%04d-%02d-%02d %02d:%02d:%02d", 
                                        (1900 + timp->tm_year), ( 1 + timp->tm_mon), timp->tm_mday,
                                        (timp->tm_hour), timp->tm_min, timp->tm_sec);

    return time_str;
}

void rlog_output(const char *log, uint16_t len)
{
    RLOG_PRINT("%.*s", len, log);
}

void rlog_osc_output(const char *data, uint16_t len)
{

}

void rlog_adapter_init(void)
{
    pthread_mutex_init(&mutex, NULL);
}

void rlog_adapter_deinit(void)
{
    pthread_mutex_destroy(&mutex);
}
