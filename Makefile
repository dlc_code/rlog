VERSION  = 1.0.0

SOURCE   = $(wildcard ./src/*.c ./example/rlog_linux_adapter.c main.c)
OBJECT   = $(patsubst %.c, %.o, $(SOURCE))

INCLUDE  = -I ./src/
INCLUDE += -I ./include

TARGET  = rlog
CC      = gcc
CFLAGS  = -Wall -g

OUTPUT  = output

$(TARGET): $(OBJECT)
	@mkdir -p $(OUTPUT)
	$(CC) $^ $(CFLAGS) -o $(OUTPUT)/$(TARGET)_$(VERSION) -lpthread

%.o: %.c
	$(CC) $(INCLUDE) $(CFLAGS) -c $< -o $@

.PHONY:clean
clean:
	@rm -rf $(OBJECT) $(OUTPUT)
